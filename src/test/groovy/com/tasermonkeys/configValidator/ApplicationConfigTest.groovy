/*
 *  Copyright 2013 James Stapleton
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.tasermonkeys.configValidator

import com.tasermonkeys.configValidator.validators.ConfigValidator
import com.tasermonkeys.configValidator.validators.GreaterThanZero
import com.tasermonkeys.configValidator.validators.IsNotEmpty
import com.tasermonkeys.configValidator.validators.IsWholeNumber
import com.tasermonkeys.configValidator.validators.MatchesRegex


class ApplicationConfigTest  extends GroovyTestCase {
    private static class TestApplicationConfig extends ApplicationConfig<TestApplicationConfig> {
        TestApplicationConfig(ConfigObject config) {
            super(config)
        }
        @Override
        ConfigValidator getConfigValidator() {
            return new ConfigValidator(expectedProperties: [
                    "a" : new ConfigValidator(name: "a", expectedProperties: [
                            "string": new IsNotEmpty(),
                            "int": new IsWholeNumber().and(new GreaterThanZero())
                    ]),
                    "string": new MatchesRegex(/worl?d/)
            ]);
        }

    }

    public void testSomething() {
        def testConfigFile = """
a.string = "Hello"
a.int = 1234
string = "world"
    """
        ConfigObject config = new ConfigSlurper().parse(testConfigFile)
        ApplicationConfig appConfig = new TestApplicationConfig(config)
    }

    public void testBadConfig() {
        def testConfigFile = """
a.string = ""
a.int = "12x4"
string = "wold"
    """
        ConfigObject config = new ConfigSlurper().parse(testConfigFile)
        try {
            ApplicationConfig appConfig = new TestApplicationConfig(config)
            fail("Should have thrown")
        } catch (IllegalStateException e) {
            assertEquals("Config file 'null' is invalid: {a: {a.string: Object is expected to not be empty.},\n" +
                    "{a.int: (Expected a whole number.) && (Invalid number to compare against.)}},\n" +
                    "{string: Value does not match regex: \"worl?d\"}", e.getMessage())
        }
    }
}
