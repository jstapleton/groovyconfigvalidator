/*
 *  Copyright 2013 James Stapleton
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.tasermonkeys.configValidator.validators

import com.tasermonkeys.configValidator.PropertyValidator

class ConfigValidator extends PropertyValidator {
    String name
    Map<String, PropertyValidator> expectedProperties = [:]

    @Override
    String validate(Object value) {
        try {
            ConfigObject config = value as ConfigObject
            def errors = expectedProperties.entrySet().collect {
                def x = value."${it.key}"
                def error = it.value.validate(x)
                if ( error )
                    "${name ? "${name}." : ""}${it.key}: ${error}"
                else
                    ""
            }.findAll {it}
            if ( errors ) {
                "{" + errors.join("},\n{") + '}'
            }
            else
                ""
        } catch (ClassCastException ignored) {
            "Expected a sub object"
        }

    }
}
