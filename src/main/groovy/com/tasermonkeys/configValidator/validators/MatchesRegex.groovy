/*
 *  Copyright 2013 James Stapleton
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.tasermonkeys.configValidator.validators

import com.tasermonkeys.configValidator.PropertyValidator

import java.util.regex.Pattern

class MatchesRegex extends PropertyValidator {
    Pattern pattern;
    MatchesRegex(Pattern pattern) {
        this.pattern = pattern
    }

    MatchesRegex(String pattern, int flags = 0) {
        this.pattern = Pattern.compile(pattern, flags)
    }

    @Override
    String validate(Object value) {
        if ( this.pattern.matcher(value as String).matches() )
            ""
        else
            "Value does not match regex: \"${pattern.toString()}\""
    }
}
