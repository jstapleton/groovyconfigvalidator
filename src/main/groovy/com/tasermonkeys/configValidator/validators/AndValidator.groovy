/*
 *  Copyright 2013 James Stapleton
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.tasermonkeys.configValidator.validators

import com.tasermonkeys.configValidator.PropertyValidator

class AndValidator extends PropertyValidator {

    List<PropertyValidator> children = []

    static PropertyValidator combine(PropertyValidator a, PropertyValidator b) {
        if (a instanceof AndValidator)
            a.add(b)
        else if (b instanceof AndValidator)
            b.add(a, 0)
        else {
            new AndValidator(children: [a, b])
        }
    }

    PropertyValidator add(PropertyValidator propertyValidator) {
        children.add(propertyValidator)
        this
    }

    PropertyValidator add(PropertyValidator propertyValidator, int index) {
        children.add(index, propertyValidator)
        this
    }


    @Override
    String validate(Object value) {
        def val = children.collect { it.validate(value) }.findAll { !it.isEmpty() }
        if (val)
            '(' + val.join(') && (') + ')'
        else
            ""
    }
}
